# Build container
FROM golang:1.11-alpine

COPY . /go/src/gitlab.com/j-velasco/s3gatekeeper
WORKDIR /go/src/gitlab.com/j-velasco/s3gatekeeper

RUN go build -o s3gatekeeper-server ./cmd/server

# Run container
FROM alpine
ARG BUILD_DATE
ARG VCS_REF

LABEL org.label-schema.name="s3gatekeeper" \
      org.label-schema.description="Web proxy to access to S3 content secured with OAuth2 and regex to match authorized emails." \
      org.label-schema.vcs-url="https://gitlab.com/j-velasco/s3gatekeeper" \
      org.label-schema.schema-version="1.0" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.vcs-ref=$VCS_REF

RUN apk add --update ca-certificates
COPY --from=0 /go/src/gitlab.com/j-velasco/s3gatekeeper/s3gatekeeper-server .

ENV PORT=9999

EXPOSE $PORT

ENTRYPOINT ["./s3gatekeeper-server"]
