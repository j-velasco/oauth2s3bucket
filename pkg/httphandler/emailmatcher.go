package httphandler

import (
	"net/http"
	"net/url"

	"regexp"

	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
	"gitlab.com/j-velasco/s3gatekeeper/pkg"
)

func NewEmailMatcherGuardMiddleware(emailRegex string, logout *url.URL, ss sessions.Store, conf *Oauth2Config) (Middleware, error) {
	html := `<html><body><a href="` + logout.String() + `">Logout</a></body></html>`
	er, err := regexp.Compile(emailRegex)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		u, ok := r.Context().Value("user").(pkg.User)
		if !ok || u == nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(html))
			return
		}

		if !er.MatchString(u.Email()) {
			redirectToConsentScreen(w, r, conf, ss)
			return
		}

		next(w, r)
	}, nil
}
