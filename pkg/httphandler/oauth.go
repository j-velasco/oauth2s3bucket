package httphandler

import (
	"crypto/rand"
	"encoding/base64"
	"net/http"

	"context"

	"github.com/gorilla/sessions"
	"github.com/sirupsen/logrus"
	"gitlab.com/j-velasco/s3gatekeeper/pkg"
	"golang.org/x/oauth2"
)

const sessionName = "oauth"

type Middleware func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc)

type Oauth2Config struct {
	*oauth2.Config
	Audience string
	UserInfo string
}

func NewUserInSessionGuardMiddleware(ss sessions.Store, conf *Oauth2Config) Middleware {
	return func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		session, err := ss.Get(r, sessionName)
		if err != nil {
			logrus.WithField("error", err).Error("Getting session")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if session.Values["user"] == nil {
			redirectToConsentScreen(w, r, conf, ss)
			return
		}

		c := r.Context()
		u, _ := session.Values["user"].(pkg.User)
		r = r.WithContext(context.WithValue(c, "user", u))
		next(w, r)
	}
}

func redirectToConsentScreen(w http.ResponseWriter, r *http.Request, conf *Oauth2Config, ss sessions.Store) {
	s, err := ss.Get(r, sessionName)
	if err != nil {
		logrus.WithField("error", err).Error("Getting session")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Generate random state
	b := make([]byte, 32)
	rand.Read(b)
	state := base64.StdEncoding.EncodeToString(b)
	s.Values["state"] = state
	s.Values["target_path"] = r.URL.Path

	err = s.Save(r, w)
	if err != nil {
		logrus.WithField("error", err).Error("Saving state in session")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	audience := oauth2.SetAuthURLParam("audience", conf.Audience)
	url := conf.AuthCodeURL(state, audience)

	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func NewOAuthCallbackHandler(ss sessions.Store, exSrv pkg.OAuthExchangeCodeService) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s, err := ss.Get(r, sessionName)
		if err != nil {
			logrus.WithField("error", err).Error("Getting session")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		stateInRequest := r.URL.Query().Get("state")
		if s.Values["state"] != stateInRequest {
			logrus.WithField("stateInRequest", stateInRequest).
				WithField("stateInSession", s.Values["state"]).
				Debug("Error comparing states")
			http.Error(w, "Bad state parameter", http.StatusBadRequest)
			return
		}
		u, err := exSrv.ExchangeCode(pkg.OAuthCode(r.URL.Query().Get("code")))
		if err != nil {
			logrus.WithField("error", err).Error("exchanging code")
			http.Error(w, "Error exchanging code", http.StatusInternalServerError)
			return
		}
		s.Values["user"] = u
		err = ss.Save(r, w, s)
		if err != nil {
			http.Error(w, "Error saving user in session", http.StatusInternalServerError)
			logrus.WithField("error", err).Error("saving user in session")
			return
		}

		redirectTo, ok := s.Values["target_path"].(string)
		if !ok {
			redirectTo = "/"
		}

		http.Redirect(w, r, redirectTo, http.StatusFound)
	}
}
