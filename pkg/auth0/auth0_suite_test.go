package auth0_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestAuth0(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Auth0 Suite")
}
