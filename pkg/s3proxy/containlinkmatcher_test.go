package s3proxy_test

import (
	"errors"
	"fmt"

	"github.com/onsi/gomega/types"
	"golang.org/x/net/html"
)

type containLinkToMatcher struct {
	href    string
	text    string
	failure string
}

func containLinkTo(href, text string) types.GomegaMatcher {
	return &containLinkToMatcher{href: href, text: text}
}

// TODO the tokenizer keep the position of the last read token, meaning that the order
// of assertion matters, fix it!
func (m *containLinkToMatcher) Match(actual interface{}) (success bool, err error) {
	tokenizer, ok := actual.(*html.Tokenizer)
	if !ok {
		return false, errors.New("containLinkToMatcher matcher expects an html.Tokenizer")
	}

	for {
		tt := tokenizer.Next()
		switch {
		// end of html
		case tt == html.ErrorToken:
			return false, nil
		case tt == html.StartTagToken:
			t := tokenizer.Token()
			if t.Data != "a" {
				continue
			}
			for _, a := range t.Attr {
				if a.Key != "href" || a.Val != m.href {
					continue
				}

				textToken, ok := m.textToken(tokenizer)
				if !ok {
					m.failure = "Link with href correct but no text found"
					return false, nil
				}
				if m.text != "" && textToken.String() != m.text {
					m.failure = fmt.Sprintf(
						"Link with href correct but wrong text \n\texpected: %s\n\tgot: %s",
						m.text,
						textToken.String(),
					)
					return false, nil
				}
				return true, nil

			}
		}
	}
}

func (m *containLinkToMatcher) textToken(tokenizer *html.Tokenizer) (html.Token, bool) {
	for {
		tt := tokenizer.Next()
		switch tt {
		case html.ErrorToken:
			return html.Token{}, false
		case html.TextToken:
			return tokenizer.Token(), true
		}
	}
}

func (m *containLinkToMatcher) FailureMessage(actual interface{}) (message string) {
	if m.failure != "" {
		return m.failure
	}
	return fmt.Sprintf("Page doesn't contain link href %s", m.href)
}

func (m *containLinkToMatcher) NegatedFailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Page shouldn't contain link href %s", m.href)
}
