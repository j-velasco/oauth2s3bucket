package s3proxy_test

import (
	"io/ioutil"

	"golang.org/x/net/html"

	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/sirupsen/logrus"

	"net/http"
	"net/http/httptest"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"gitlab.com/j-velasco/s3gatekeeper/pkg/s3proxy"
)

const (
	existingFileContent = "Hello world!\n"
	bucketName          = "bucket"
	prefix              = "dir"
)

var _ = Describe("Proxy", func() {
	var (
		proxy http.HandlerFunc
		rr    *httptest.ResponseRecorder
	)
	logrus.SetOutput(ioutil.Discard)

	BeforeEach(func() {
		proxy = s3proxy.NewS3Proxy(makeS3Client(), bucketName, prefix)
		rr = httptest.NewRecorder()
	})

	It("gives file content", func() {
		r := httptest.NewRequest(http.MethodGet, "/existing_file.txt", nil)
		proxy(rr, r)

		html, _ := ioutil.ReadAll(rr.Result().Body)
		Expect(string(html[:])).To(Equal(existingFileContent))
		Expect(rr.Result().StatusCode).Should(Equal(http.StatusOK))
	})

	It("gives 404 for non existing object", func() {
		r := httptest.NewRequest(http.MethodGet, "/non_existing_file.txt", nil)
		proxy(rr, r)

		Expect(rr.Result().StatusCode).Should(Equal(http.StatusNotFound))
	})

	It("lists directory content", func() {
		r := httptest.NewRequest(http.MethodGet, "/", nil)
		proxy(rr, r)

		Expect(rr.Result().StatusCode).Should(Equal(http.StatusOK))

		page := html.NewTokenizer(rr.Result().Body)
		Expect(page).To(containLinkTo("/subdir/", "subdir/"))
		Expect(page).To(containLinkTo("/existing_file.txt", "existing_file.txt"))
	})

	It("lists subdirectories without directory prefix", func() {
		r := httptest.NewRequest(http.MethodGet, "/subdir", nil)
		proxy(rr, r)

		Expect(rr.Result().StatusCode).Should(Equal(http.StatusOK))

		page := html.NewTokenizer(rr.Result().Body)
		Expect(page).To(containLinkTo("/subdir/.keep", ".keep"))
	})
})

func makeS3Client() *s3.S3 {
	config := &aws.Config{
		Endpoint:         aws.String("http://minio-server:9000"),
		DisableSSL:       aws.Bool(true),
		Region:           aws.String("us-east-1"),
		S3ForcePathStyle: aws.Bool(true),
		Credentials: credentials.NewStaticCredentials(
			"AKIAIOSFODNN7EXAMPLE",
			"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY",
			"",
		),
	}
	sess, err := session.NewSession(config)
	if err != nil {
		Fail(err.Error())
	}
	return s3.New(sess)
}
