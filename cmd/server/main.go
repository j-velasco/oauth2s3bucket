package main

import (
	"os"

	"net/http"

	"context"
	"os/signal"
	"time"

	_ "github.com/joho/godotenv/autoload"
	"github.com/sirupsen/logrus"
	"gitlab.com/j-velasco/s3gatekeeper/pkg/auth0"
	"gitlab.com/j-velasco/s3gatekeeper/pkg/httphandler"
	"gitlab.com/j-velasco/s3gatekeeper/pkg/httphandler/session"
	"golang.org/x/oauth2"
)

func main() {
	sessionSecret := os.Getenv("SESSION_SECRET")
	if sessionSecret == "" {
		panic("Provide a non empty SESSION_SECRET")
	}

	ss := session.NewStore(sessionSecret)
	conf := oauth2ConfFromEnv()
	exSrv := auth0.NewOAuthExchangeCodeService(conf)
	h := NewHandler(ss, exSrv, conf)
	port := os.Getenv("PORT")
	if os.Getenv("DEBUG") == "1" {
		logrus.Info("Debug mode enable")
		logrus.SetLevel(logrus.DebugLevel)
	}

	s := http.Server{Addr: ":" + port, Handler: h}

	// subscribe to SIGINT signals
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)
	go func() {
		logrus.WithField("server_address", s.Addr).Info("Starting server")
		if err := s.ListenAndServe(); err != nil {
			logrus.WithField("error", err).Fatal("Error starting server")
			stopChan <- os.Interrupt
		}
	}()

	<-stopChan
	logrus.Info("Shutting down server...")

	// shut down gracefully, but wait no longer than 5 seconds before halting
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	s.Shutdown(ctx)

	logrus.Info("Server stopped")
}

func oauth2ConfFromEnv() *httphandler.Oauth2Config {
	d := os.Getenv("AUTH0_DOMAIN")

	return &httphandler.Oauth2Config{
		Config: &oauth2.Config{
			ClientID:     os.Getenv("AUTH0_CLIENT_ID"),
			ClientSecret: os.Getenv("AUTH0_CLIENT_SECRET"),
			RedirectURL:  os.Getenv("AUTH0_CALLBACK_URL"),
			Scopes:       []string{"openid", "profile", "email"},
			Endpoint: oauth2.Endpoint{
				AuthURL:  endpoint(d, "/authorize"),
				TokenURL: endpoint(d, "/oauth/token"),
			},
		},
		Audience: os.Getenv("AUTH0_AUDIENCE"),
		UserInfo: endpoint(d, "/userinfo"),
	}
}

func endpoint(domain, path string) string {
	return "https://" + domain + path
}
